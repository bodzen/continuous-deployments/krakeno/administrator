# Administrator Helm Chart

Continuous deployement for the project [Administrator](https://gitlab.com/bodzen/krakeno/administrator)

## Requirements

- helm 3
- kubectl >= v1.16.3
